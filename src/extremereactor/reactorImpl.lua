---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by Bahak.
--- DateTime: 15/08/2018 10:26 AM
---
local rods = require("extremereactor.reactorRod")
local ReactorStatus = require("extremereactor.reactorStatus")

local function init(self, address, id)
    assert(self._base)
    local protected = self._base.init(self, address, id)

    -- Private members
    local mRods = {}
    local mRun = false
    local mManualOverride = false

    -- Public members
    self.mOperatingTemperature = 800
    self.mOperatingReactivity = 400
    self.mOverheatingTemperature = 1000
    self.mCriticalTemperature = 1500
    self.mStatus = ReactorStatus()

    function self.turnOn()
        mRun = true
        return self.activate()
    end
    function self.turnOff()
        mRun = false
        self.shutdown()
    end

    function self.activate()
        if not mRun then return nil end
        if protected.mComponent.getActive() == true then return nil end
        -- Wait until the Reactor is no longer overheating before restarting it.
        if self.getCoreTemperature() >= self.mOverheatingTemperature then
            return "Activation is DENIED. Reactor is overheating."
        end

        -- Start with all rods at 50 percent then auto adjust them afterwards.
        if not mManualOverride then
            self.adjustRods(100)
            self.adjustRods(-50)
        end
        self.mStatus.reset()
        protected.mComponent.setActive(true)

        return nil
    end

    function self.shutdown()
        protected.mComponent.setActive(false)

        if not mManualOverride then
            -- Force all Rods in for security reasons.
            self.adjustRods(100)
        end
    end

    function self.isActive()                                           return protected.mComponent.getActive()    end
    function self.isManualOverride()                        return mManualOverride  end
    function self.setManualOverride(enable)         mManualOverride = enable  end

    function self.getCasingTemperature()               return protected.mComponent.getCasingTemperature()  end
    function self.getCoreTemperature()                  return protected.mComponent.getFuelTemperature()    end
    function self.getOverheatingTemperature()    return self.mOverheatingTemperature  end
    function self.getCriticalTemperature()              return self.mCriticalTemperature   end
    function self.getOperatingTemperature()        return self.mOperatingTemperature  end
    function self.getOperatingReactivity()              return self.mOperatingReactivity  end

    -- Fuel
    function self.getFuelStats()                    return protected.mComponent.getFuelStats() end
    function self.getFuelCapacity()               return protected.mComponent.getFuelAmountMax()    end
    function self.getFuelAmount()               return protected.mComponent.getFuelAmount()    end
    function self.getFuelBurnupRate()       return protected.mComponent.getFuelConsumedLastTick()    end
    function self.getFuelReactivity()           return protected.mComponent.getFuelReactivity()    end
    function self.getWasteAmount()           return protected.mComponent.getWasteAmount()    end

    -- Coolant
    function self.getWaterStats()
        return protected.mComponent.getCoolantFluidStats()
    end

    function self.getWaterAmount()
        return protected.mComponent.getCoolantAmount()
    end

    function self.getWaterCapacity()
        return protected.mComponent.getCoolantAmountMax()
    end

    -- Steam output
    function self.getSteamStats()
        return protected.mComponent.getHotFluidStats()
    end

    function self.getSteamAmount()
        return protected.mComponent.getHotFluidAmount()
    end

    function self.getSteamCapacity()
        return protected.mComponent.getHotFluidAmountMax()
    end

    function self.getSteamOutput()
        return protected.mComponent.getHotFluidProducedLastTick()
    end

    -- Control Rods

    -- Adjust all rods at once.
    function self.adjustRods(offset)
        for i=1, mRods.size() do
            self.adjustRod(i, offset)
        end
    end

    -- Adjust only one rod
    -- This will give a more fine temperature tuning
    function self.adjustRod(index, offset)
        local rod = mRods[index]
        local newLevel = math.min(100, math.max(0, rod.mLevel + offset))
        self.setRodLevel(index, newLevel)
        self.mStatus.reset()
    end

    -- Adjust the lowest or the highest rod depending on the offset given
    -- This will give the finest temperature tuning
    function self.adjustRodSmart(offset)
        local index = 1
        local optimalLevel = 0
        for i = 1, mRods.size() do
            local rodLevel = mRods[i].mLevel
            if (offset < 0 and rodLevel > optimalLevel) or
                    (offset > 0 and mRods[i].mLevel < optimalLevel) then
                index = i
                optimalLevel = rodLevel
            end
        end

        self.adjustRod(index, offset)
    end

    function self.updateRods()
        if self.getNumberOfRods() ~= mRods.size() then
            rods.initRods()
        else
            for i=1, mRods.size() do
                local rod = mRods[i]
                rod.mName = self.getRodName(rod.mIndex)
                rod.mLevel = self.getRodLevel(rod.mIndex)
            end
        end
    end

    function self.getNumberOfRods()
        return protected.mComponent.getNumberOfControlRods()
    end

    -- index can be a position number or the name of the rod
    function self.getRod(index)
        if type(index) == "number" then
            assert(index > 0 and index <= mRods.size())
        end
        return mRods[index]
    end

    -- Get Average level for all rods
    function self.getAverageRodsLevel()
        if mRods.size() == 0 then return 0 end

        local average = 0
        for i=1, mRods.size() do
            average = average + protected.mComponent.getControlRodLevel(i)
        end

        return math.floor(average / mRods.size())
    end

    -- index can be a position number or the name of the rod
    function self.getRodLevel(index)
        local componentRodIndex = 0
        if type(index) == "number" then
            assert(index > 0 and index <= protected.mComponent.getNumberOfControlRods())
            componentRodIndex = index-1
        else
            componentRodIndex = mRods[index].mIndex-1
        end

        return protected.mComponent.getControlRodLevel(componentRodIndex)
    end

    function self.setRodLevel(index, newLevel)
        local componentRodIndex = 0
        if type(index) == "number" then
            assert(index > 0 and index <= protected.mComponent.getNumberOfControlRods())
            componentRodIndex = index-1
        else
            componentRodIndex = mRods[index].mIndex-1
        end

        protected.mComponent.setControlRodLevel(componentRodIndex, newLevel)
        mRods[index].mLevel = protected.mComponent.getControlRodLevel(componentRodIndex)
    end

    -- index can be a position number
    function self.getRodName(index)
        assert(type(index) == "number")
        assert(index > 0 and index <= protected.mComponent.getNumberOfControlRods())

        return protected.mComponent.getControlRodName(index - 1)
    end

    mRods = rods.initRods(self)
end


return init