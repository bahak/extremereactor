local factory = require("componentFactory")
local BaseComponent = require("baseComponent")
local init = require("extremereactor.turbineImpl")
require("common.class")

local Turbine = class(BaseComponent, init)
Turbine._TYPE = "br_turbine"
Turbine._CLASS = "Turbine"
Turbine._ID_PREFIX = "T"

local turbineFactory = {}

function turbineFactory.discover()
    return factory.discover(Turbine)
end

return turbineFactory