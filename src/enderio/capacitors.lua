local factory = require("componentFactory")
local BaseComponent = require("baseComponent")
local init = require("enderio.capacitorImpl")
require("common.class")


local Capacitor = class(BaseComponent, init)
Capacitor._TYPE = "capacitor_bank"
Capacitor._CLASS = "Capacitor"
Capacitor._ID_PREFIX = "C"

local capacitorFactory = {}

function capacitorFactory.discover()
    return factory.discover(Capacitor)
end

return capacitorFactory