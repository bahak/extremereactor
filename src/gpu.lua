---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by Bahak.
--- DateTime: 19/08/2018 12:54 PM
---
local gpu = {}
local backgroundColor = 0x000000

function gpu.getResolution()
    return 160,40
end

function gpu.fill(...)
end

function gpu.setBackground(color)
    backgroundColor = color
end

function gpu.set(x, y, msg)
    print(msg)
end

return gpu