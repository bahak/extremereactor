-- OpenComputer component Table Mockup
local gpu = require("gpu")
local component = {["b988cd9f-621b-4194-993c-0939913c8392"]="br_turbine",
                   ["ce27d48f-d652-417d-bd5e-e9c7398f39cd"]="filesystem",
                   ["d4060cc8-85a4-4a0b-9e39-b687cd84380f"]="capacitor_bank",
                   ["512954a3-cc70-44c0-84e7-96326efa83a2"]="keyboard",
                   ["d413b589-1d41-41b0-9be9-050365e54c28"]="eeprom",
                   ["525af179-36a9-46e3-bfbc-e8cead1635e6"]="screen",
                   ["f9f05f68-c5ca-479f-9240-ecae3e729c1f"]="capacitor_bank",
                   ["944b970a-c84c-4ce7-8dbd-290772b86eb9"]="internet",
                   ["fa8ba21d-c460-4f81-9690-b0d1b7c38810"]="capacitor_bank",
                   ["4bf4c2d4-b1e7-47b2-8a85-d8eaff634ce3"]="br_reactor",
                   ["380ee1fa-1ee3-4216-8980-193edc3c06cc"]="filesystem",
                   ["6d87f39e-97e8-4391-9581-6d97c4703c2f"]="computer",
                   ["7372f014-246e-4b6f-b5b6-a2bbe804f568"]="br_turbine",
                   ["ce977bed-745a-412d-ac3c-e97bdb5defea"]="capacitor_bank",
                   ["666fef9c-7056-4184-b964-545e904fe41a"]="br_turbine",
                   ["c2e2c745-bce1-469e-abc4-bf9609a1d170"]="br_turbine",
                   ["aa998a4e-3125-48d3-b263-a198f150e60b"]="gpu",
                   ["gpu"]=gpu
                    }


function component.list(filter)
    if (filter ~= {}) then
        local filteredComponents = {}
        for address, type in pairs(component) do
            if (type == filter) then
                filteredComponents[address] = type
            end
        end
        return filteredComponents
    end

    return component
end

function component.proxy(address)
    local obj = {
        mActive = false,
        mAddress = address,
        mReactivity = 300
    }

    local type =component[address]
    local rods = {[0]=70, [1]=70, [2]=70, [3]=70}
    if type == "br_reactor" then
        function obj.getActive()
            return obj.mActive
        end
        function obj.setActive(active)
            obj.mActive = active
        end
        function obj.getFuelStats()
            return {
                fuelAmount = 60988.0,
                fuelCapacity = 96000.0,
                fuelConsumedLastTick = 0.0,
                fuelReactivity = 0.0,
                fuelTemperature = 20.0,
                wasteAmount = 12.0
            }
        end

        function obj.getFuelReactivity()
            return obj.mReactivity+1
        end
        function obj.getFuelAmountMax()
            return 96000
        end
        function obj.getHotFluidAmount()
            return 1000
        end
        function obj.getFuelTemperature()
            return 800
        end
        function obj.getWasteAmount()
            return 12
        end

        function obj.getHotFluidStats()
            return {
                fluidAmount = 0.0,
                fluidCapacity = 24200.0,
                fluidProducedLastTick = 0.0
            }
        end

        function obj.getCoolantFluidStats()
            return {
                fluidAmount = 24200.0,
                fluidCapacity = 24200.0,
                fluidType = "water"
            }
        end

        function obj.getNumberOfControlRods()
            return 4
        end
        function obj.getControlRodsLevels()
            return rods
        end
        function obj.getControlRodLevel(index)
            return rods[index]
        end
        function obj.setControlRodLevel(index, level)
            rods[index] = level
        end
        function obj.getControlRodName(index)
            local rodNames = {[0]="rn", [1]="rs", [2]="re", [3]="rw"}
            return rodNames[index]
        end
    elseif type == "br_turbine" then
        function obj.getRotorSpeed()
            return 1857
        end
    elseif type == "capacitor_bank" then
        function obj.getAverageOutputPerTick()
            return 30
        end
    end
    return obj
end

return component
