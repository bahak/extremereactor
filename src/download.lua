local fs = require"filesystem"
local wget=loadfile("/bin/wget.lua")
local cd=loadfile("/bin/cd.lua")

wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/baseComponent.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/componentFactory.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/modulelist.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/run.lua")

local COMMON="/home/common"
local ENDERIO="/home/enderio"
local EXTREMEREACTOR="/home/extremereactor"
local GUI="/home/gui"

if not fs.exists(COMMON) then
    fs.makeDirectory(COMMON)
end
if not fs.exists(ENDERIO) then
    fs.makeDirectory(ENDERIO)
end
if not fs.exists(EXTREMEREACTOR) then
    fs.makeDirectory(EXTREMEREACTOR)
end
if not fs.exists(GUI) then
    fs.makeDirectory(GUI)
end

cd(COMMON)
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/common/array.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/common/class.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/common/queue.lua")

cd(ENDERIO)
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/enderio/capacitors.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/enderio/capacitorImpl.lua")

cd(EXTREMEREACTOR)
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/extremereactor/turbines.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/extremereactor/turbineImpl.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/extremereactor/reactor.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/extremereactor/reactorStatus.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/extremereactor/reactorImpl.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/extremereactor/reactorController.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/extremereactor/reactorRod.lua")

cd(GUI)
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/gui/gui.lua")
wget("-f", "https://bitbucket.org/bahak/extremereactor/raw/master/src/gui/buttonapi.lua")

cd("/home")