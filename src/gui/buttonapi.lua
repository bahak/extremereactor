local API = {}
local button={}

local component = require("component")
local colors = require("colors")
local term = require("term")
local mon = component.gpu
local w, h = mon.getResolution()
local Green = 0x00AA00
local Red = 0xAA0000
local Black = 0x000000

function API.clearTable()
  button = {}
  API.clear()
end

-- Hide a button
function API.clear(bData)
  mon.setBackground(Black)

  if bData then
    mon.fill(bData["xmin"], bData["ymin"], (bData["xmax"]-bData["xmin"]+1), (bData["ymax"]-bData["ymin"]+1), " ")
  else
    mon.fill(1, 1, w, h, " ")
  end
end
               
function API.setTable(name, func, arg, xmin, xmax, ymin, ymax)
  button[name] = {}
  button[name]["displayText"] = name
  button[name]["func"] = func
  button[name]["arg"] = arg
  button[name]["active"] = false
  button[name]["xmin"] = xmin
  button[name]["ymin"] = ymin
  button[name]["xmax"] = xmax
  button[name]["ymax"] = ymax
  button[name]["enabled"] = false
end

-- Allow to change text in the button
function API.setText(name, displayText)
  button[name]["displayText"] = displayText
end

function API.fill(text, color, bData)
  local yspot = math.floor((bData["ymin"] + bData["ymax"]) /2)
  local xspot = math.floor((bData["xmax"] + bData["xmin"] - string.len(text)) /2)+1
  local oldColor = mon.setBackground(color)
  mon.fill(bData["xmin"], bData["ymin"], (bData["xmax"]-bData["xmin"]+1), (bData["ymax"]-bData["ymin"]+1), " ")
  mon.set(xspot, yspot, text)
  mon.setBackground(oldColor)
end

function API.screen()
  for name,_ in pairs(button) do
    API.redraw(name)
  end
end

function API.redraw(name)
  local currColor
  local data = button[name]
  if data["enabled"] == true then
    if data["active"] == true then currColor = Green else currColor = Red end
    API.fill(data["displayText"], currColor, data)
  else
    API.clear(data)
  end
end

function API.enableButton(name, enable)
  button[name]["enabled"] = enable
end

function API.showButton(name)
  API.enableButton(name, true)
  API.redraw(name)
end

function API.hideButton(name)
  API.enableButton(name, false)
  API.redraw(name)
end

function API.isActive(name)
    return button[name]["active"]
end

function API.toggleButton(name)
  local data = button[name]
  data["active"] = not data["active"]
  API.redraw(name)

  -- Returns the new state of the button
  return data["active"]
end     

function API.flash(name,length)
  API.toggleButton(name)
  os.sleep(length)
  API.toggleButton(name)
end
                                             
function API.checkxy(x, y)
  for name, data in pairs(button) do
    if y>=data["ymin"] and  y <= data["ymax"] then
      if x>=data["xmin"] and x<= data["xmax"] and data["enabled"] then
        data["func"](name, data["arg"], x ,y)
          return true
      end
    end
  end
  return false
end
     
function API.heading(text)
  w, h = mon.getResolution()
  term.setCursor((w-string.len(text))/2+1, 1)
  term.write(text)
end
     
function API.label(x, y, text)
  term.setCursor(x, y)
  term.write(text)
end

return API





--eof